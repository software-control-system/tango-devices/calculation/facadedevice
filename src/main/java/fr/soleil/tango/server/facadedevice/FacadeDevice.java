package fr.soleil.tango.server.facadedevice;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.tango.DeviceState;
import org.tango.server.InvocationContext;
import org.tango.server.InvocationContext.ContextType;
import org.tango.server.ServerManager;
import org.tango.server.annotation.AroundInvoke;
import org.tango.server.annotation.Attribute;
import org.tango.server.annotation.Delete;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DeviceProperty;
import org.tango.server.annotation.DynamicManagement;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.State;
import org.tango.server.annotation.Status;
import org.tango.server.annotation.TransactionType;
import org.tango.server.dynamic.DynamicManager;
import org.tango.server.dynamic.attribute.MultiAttributeProxy;
import org.tango.server.dynamic.attribute.ProxyAttribute;
import org.tango.server.dynamic.command.ProxyCommand;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.Tango.DispLevel;
import fr.esrf.TangoApi.CommandInfo;
import fr.esrf.TangoApi.DeviceProxy;

@Device(transactionType = TransactionType.NONE)
public final class FacadeDevice {
    private static final String STATUS_NAME = "Status";
    private static final String STATE_NAME = "State";
    private final Logger logger = LoggerFactory.getLogger(FacadeDevice.class);
    private final XLogger xlogger = XLoggerFactory.getXLogger(FacadeDevice.class);

    /**
     * MAIN
     */
    public static void main(final String[] args) {
        ServerManager.getInstance().start(args, FacadeDevice.class);
    }

    /**
     * The commands label list
     */
    private String[] attributeNames;

    @DeviceProperty(isMandatory = true, description = "the list of attributes to create. syntax: \"attributeName,my/source/attribute/name,R(read only,optional)\" or * for all attributes of DeviceProxy property")
    private String[] attributeNameList;
    //    @DeviceProperty(description = "if true, the initialization of the device is done until OK", defaultValue = "false")
    //    private boolean autoReconnection = false;

    @DeviceProperty(description = "optional. the list of commands to create. syntax: \"commandName,my/source/command/name\" or * for all commands of DeviceProxy property")
    private String[] commandNameList;

    @DeviceProperty(isMandatory = true, description = "The name of the device on which the state is mapped")
    private String deviceProxy;

    /**
     * The deviceProxy in the case of using *
     */
    private DeviceProxy proxy = null;

    @DynamicManagement
    private DynamicManager dynMngt;

    private volatile boolean initialized = false;

    /**
     * The table of errors <AttributName, Error Reason>
     */
    private final Map<String, String> mapAttributReportTable = Collections
            .synchronizedMap(new HashMap<String, String>());

    /**
     * The table of error on command <CommandName, Error Reason>
     */
    private final Map<String, String> mapCommandReportTable = Collections
            .synchronizedMap(new HashMap<String, String>());
    /**
     * SimpleDateFormat to timeStamp the errors message
     */
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    /**
     * The state of the device
     */

    @State
    private DeviceState state = DeviceState.FAULT;

    @Status
    private String status = "";

    @Attribute(displayLevel = DispLevel._EXPERT)
    private final String version = "3.0.5";

    private MultiAttributeProxy multiAttributeProxy;
    private boolean multiUpdate = false;

    //    private final Executor exec = Executors.newSingleThreadExecutor();

    //    public FacadeDevice() {
    //        exec.execute(new Runnable() {
    //            @Override
    //            public void run() {
    //                while (true) {
    //                    if (!initialized && autoReconnection) {
    //                        try {
    //                            Thread.sleep(3000);
    //                        } catch (final InterruptedException e1) {
    //                            // TODO Auto-generated catch block
    //                            e1.printStackTrace();
    //                        }
    //                        logger.debug("The device is not initialized");
    //                        try {
    //                            deleteDevice();
    //                            initDevice();
    //                        } catch (final DevFailed e) {
    //                            logger.error("error", e);
    //                            logger.error(DevFailedUtils.toString(e));
    //                        }
    //                    }
    //                }
    //            }
    //        });
    //    }

    /**
     * Creation of attribute if is possible else throw exception
     *
     * @throws DevFailed
     */
    private void createAttributes() throws DevFailed {
        xlogger.entry();
        checkPropertiesForAttributes();
        if (attributeNameList[0].trim().equals("*") && proxy != null) {
            // all attributes are on the same device, will use readAttributes in
            // around invoke
            multiUpdate = true;
            attributeNames = proxy.get_attribute_list();
            for (final String attributeName : attributeNames) {
                if (!attributeName.equalsIgnoreCase(STATE_NAME) && !attributeName.equalsIgnoreCase(STATUS_NAME)) {
                    logger.debug("create {}", attributeName);
                    dynMngt.addAttribute(new ProxyAttribute(attributeName, proxy.get_name() + "/" + attributeName,
                            false, false));
                }
            }
        } else {
            createMultiAttributes();
        }
        xlogger.exit();
    }

    private void createMultiAttributes() throws DevFailed {
        final List<String> listAttr = Arrays.asList(attributeNameList);
        final Set<String> setAttr = new HashSet<String>(listAttr);
        for (final String attributeName : setAttr) {
            // Now all the information is in the setAttr
            // syntax must be attributeLabel , attributeName(, R)
            // So count the token sperated by ","
            final StringTokenizer tokens = new StringTokenizer(attributeName.trim(), ",");
            final int nbTokens = tokens.countTokens();
            if (nbTokens < 2) {
                mapAttributReportTable
                .put(attributeName.trim(),
                        simpleDateFormat.format(new Date())
                        + " : Wrong definition for attributeNameList property, it must be : attributeLabel , attributeName(, R)");
            } else {
                boolean readOnly = false;
                // If there is more than 2 token, the R option is defined
                if (nbTokens > 2) {
                    readOnly = true;
                }
                final String attributeLabel = tokens.nextToken().trim();
                final String attributeFullName = tokens.nextToken().trim();
                if (!attributeName.equalsIgnoreCase(STATE_NAME) && !attributeName.equalsIgnoreCase(STATUS_NAME)) {
                    logger.debug("create {} with read only option= {}", attributeName, readOnly);
                    dynMngt.addAttribute(new ProxyAttribute(attributeLabel, attributeFullName, readOnly));
                }
            }
        }
    }

    private void checkPropertiesForAttributes() throws DevFailed {
        if (attributeNameList[0].trim().equals("*") && proxy == null) {
            DevFailedUtils.throwDevFailed("ATTRIBUTE_CONFIG_ERROR", "Unknown device proxy");
        }
    }

    /**
     * Creation of command if is possible else throw exception
     *
     * @throws DevFailed
     */
    private void createCommands() throws DevFailed {
        xlogger.entry();

        if (commandNameList.length > 0 && commandNameList[0].trim().equals("*") && proxy != null) {
            final CommandInfo[] tmpCommandList = new CommandInfo[0];// proxy.command_list_query();
            final String deviceName = proxy.get_name();
            for (final CommandInfo commandInfo : tmpCommandList) {
                final String cmdName = commandInfo.cmd_name;
                final String commandFullName = deviceName + "/" + cmdName;
                if (cmdName.equalsIgnoreCase("Init")) {
                    logger.debug("create command InitDevice");
                    dynMngt.addCommand(new ProxyCommand("InitDevice", commandFullName));
                } else if (!cmdName.equalsIgnoreCase(STATE_NAME) && !cmdName.equalsIgnoreCase(STATUS_NAME)) {
                    logger.debug("create command {}", commandFullName);
                    dynMngt.addCommand(new ProxyCommand(cmdName, commandFullName));
                }
            }
        } else {
            final List<String> listCmd = Arrays.asList(commandNameList);
            for (final String commandName : new HashSet<String>(listCmd)) {
                final StringTokenizer tokens = new StringTokenizer(commandName.trim(), ",");
                final int nbTokens = tokens.countTokens();
                if (nbTokens != 2) {
                    xlogger.exit();
                    DevFailedUtils
                    .throwDevFailed("COMMAND_CONFIG_ERROR",
                            "Wrong definition for commandNameList property, it must be : \n command label,command name");
                }

                final String commandLabel = tokens.nextToken().trim();
                final String commandFullName = tokens.nextToken().trim();
                logger.debug("create command {}", commandFullName);
                dynMngt.addCommand(new ProxyCommand(commandLabel, commandFullName));
            }
        }
        xlogger.exit();
    }

    /**
     * Throw exception if the creation is failed
     *
     * @throws DevFailed
     */
    private void createDeviceProxy() throws DevFailed {
        xlogger.entry();
        if (!deviceProxy.trim().isEmpty()) {
            proxy = new DeviceProxy(deviceProxy);
            proxy.read_attribute("State");
        } else {
            xlogger.exit();
            DevFailedUtils.throwDevFailed("CONFIG_ERROR", "The property \"deviceProxy\" is not set");
        }
        xlogger.exit();
    }

    /**
     * Delete device
     *
     * @throws DevFailed
     */
    @Delete
    public void deleteDevice() throws DevFailed {
        xlogger.entry();
        initialized = false;
        dynMngt.clearAll();
        mapCommandReportTable.clear();
        mapAttributReportTable.clear();
        xlogger.exit();
    }

    /**
     * The detected error on the attribute list
     *
     * @return errorAttributeReport
     */
    @Attribute(displayLevel = DispLevel._EXPERT)
    public String[] getErrorAttributeReport() {
        xlogger.entry();
        int i = 0;
        String[] errorAttributeReport;
        if (mapAttributReportTable.size() > 0) {
            errorAttributeReport = new String[mapAttributReportTable.size()];
            for (final Entry<String, String> oneErrorAttributeReport : mapAttributReportTable.entrySet()) {
                errorAttributeReport[i++] = oneErrorAttributeReport.getKey() + " -> "
                        + oneErrorAttributeReport.getValue();
            }
        } else {
            errorAttributeReport = new String[] { "No error" };
        }
        xlogger.exit();
        return errorAttributeReport;
    }

    /**
     * The detected errors on the command list
     *
     * @return errorCommandReport
     */
    @Attribute(displayLevel = DispLevel._EXPERT)
    public String[] getErrorCommandReport() {
        xlogger.entry();
        int i = 0;
        String[] errorCommandReport;
        if (mapCommandReportTable.size() > 0) {
            errorCommandReport = new String[mapCommandReportTable.size()];
            for (final Entry<String, String> oneErrorCommandReport : mapCommandReportTable.entrySet()) {
                errorCommandReport[i++] = oneErrorCommandReport.getKey() + " -> " + oneErrorCommandReport.getValue();
            }
        } else {
            errorCommandReport = new String[] { "No error" };
        }
        xlogger.exit();
        return errorCommandReport;
    }

    /**
     * Update state with the state of device defined in deviceProxy property
     *
     * @return DeviceState State
     * @throws DevFailed
     */
    public DeviceState getState() throws DevFailed {
        xlogger.entry();
        if (initialized && !multiUpdate) {
            logger.debug("update state");
            state = DeviceState.getDeviceState(proxy.state());
        }
        xlogger.exit();
        return state;
    }

    public String getStatus() throws DevFailed {
        if (initialized && !multiUpdate) {
            logger.debug("update status");
            status = proxy.status();
        }
        return status;
    }

    @AroundInvoke
    public void aroundInvoke(final InvocationContext ctx) throws DevFailed {
        // if the attributes are all on the same proxy, read them once with
        // read_attributes
        if (initialized && multiUpdate && ctx.getContext().equals(ContextType.PRE_READ_ATTRIBUTES)) {
            final String[] names = ctx.getNames();
            String[] namesLower = new String[names.length];
            for (int i = 0; i < namesLower.length; i++) {
                namesLower[i] = names[i].toLowerCase(Locale.ENGLISH);
            }
            namesLower = ArrayUtils.removeElement(namesLower, "errorattributereport");
            namesLower = ArrayUtils.removeElement(namesLower, "errorcommandreport");
            logger.debug("update attributes {}", Arrays.toString(names));
            final org.tango.server.attribute.AttributeValue[] result = multiAttributeProxy.readAttributes(namesLower);
            // update state and status
            for (int i = 0; i < names.length; i++) {
                if (names[i].equalsIgnoreCase(STATE_NAME)) {
                    state = DeviceState.getDeviceState((DevState) result[i].getValue());
                } else if (names[i].equalsIgnoreCase(STATUS_NAME)) {
                    status = result[i].getValue().toString();
                }
            }
        }
    }

    /**
     * Init device
     *
     * @throws DevFailed
     */
    @Init
    public void initDevice() throws DevFailed {
        xlogger.entry();
        createDeviceProxy();
        createAttributes();
        createCommands();
        multiAttributeProxy = new MultiAttributeProxy(proxy, dynMngt);
        state = DeviceState.ON;
        status = "The device is initialized";
        initialized = true;
        logger.debug("init OK");
        xlogger.exit();
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public String getVersion() {
        return version;
    }

    public void setState(final DeviceState state) {
        this.state = state;
    }

    public void setDynMngt(final DynamicManager dynMngt) {
        this.dynMngt = dynMngt;
    }

    public void setAttributeNameList(final String[] attributeNameList) throws DevFailed {
        if (attributeNameList.length <= 0 || attributeNameList[0].trim().equals("")) {
            DevFailedUtils.throwDevFailed("ATTRIBUTE_CONFIG_ERROR", "The property \"attributeNameList\" is not set");
        }
        this.attributeNameList = Arrays.copyOf(attributeNameList, attributeNameList.length);
    }

    //    public void setAutoReconnection(final boolean autoReconnection) {
    //        this.autoReconnection = autoReconnection;
    //    }

    public void setDeviceProxy(final String deviceProxy) {
        this.deviceProxy = deviceProxy;
    }

    public void setCommandNameList(final String[] commandNameList) throws DevFailed {
        if (commandNameList.length == 0 || commandNameList[0].trim().equals("")) {
            this.commandNameList = new String[0];
        } else {
            this.commandNameList = Arrays.copyOf(commandNameList, commandNameList.length);
        }
    }
}